package com.example.v3_spring;

import com.example.v3_spring.services.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class RobotApplicationTests {

  private Client client;

  @Autowired
  private void setClient(Client client) {
    this.client = client;
  }

  @Test
  void grtz() {
    assertTrue(client.greet().toLowerCase().contains("spring"));
  }

}
