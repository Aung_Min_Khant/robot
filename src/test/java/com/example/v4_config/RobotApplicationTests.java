package com.example.v4_config;

import com.example.v4_config.services.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class RobotApplicationTests {
    private Client client;

    @Autowired
    private void setClient(Client client) {
        this.client = client;
    }

    @Test
    void grtz() {
        assertTrue(client.greet().toLowerCase().contains("config"));
    }
}
