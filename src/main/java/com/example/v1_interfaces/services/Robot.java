package com.example.v1_interfaces.services;

public interface Robot {
  String getName();
}
