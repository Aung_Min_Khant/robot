package com.example.v3_spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Client {
  private final Robot robot;

@Autowired // annotation can be ommitted if there is only one constructor
  public Client(Robot robot) {
    this.robot = robot;
  }

  public String greet() {
    return "Hello robot " + robot.getName();
  }
}
