package com.example.v3_spring.services;

public interface Robot {
  String getName();
}
