package com.example.v3_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobotApplication {

  // test using test/java/com/example/spring/RobotApplicationTests.java
  public static void main(String[] args) {
    SpringApplication.run(RobotApplication.class, args);
  }

}
