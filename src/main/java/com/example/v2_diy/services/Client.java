package com.example.v2_diy.services;

public class Client {
  private final Robot robot;


  public Client(Robot robot) {
    this.robot = robot;
  }

  public String greet() {
    return "Hello robot " + robot.getName();
  }
}
