package com.example.v4_config;

import com.example.v4_config.services.Bot;
import com.example.v4_config.services.Robot;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyApplicationConfiguration {
    @Bean
    public Robot myRobot(){
        Robot bot = new Bot("config");
        return bot;
    }
}
