package com.example.v4_config.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Client {
  private final com.example.v4_config.services.Robot robot;

  @Autowired // annotation can be ommitted if there is only one constructor
  public Client(Robot robot) {
    this.robot = robot;
  }

  public String greet() {
    return "Hello robot " + robot.getName();
  }
}
