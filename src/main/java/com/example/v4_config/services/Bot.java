package com.example.v4_config.services;


public class Bot implements Robot{
    private String name;
    public Bot(String name){
        this.name = name;
    }
    @Override
    public String getName() {
        return "My name is " + name;
    }
}
