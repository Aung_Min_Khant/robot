package com.example.v4_config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobotApplication {
    public static void main(String[] args) {
        SpringApplication.run(com.example.v4_config.RobotApplication.class, args);
    }
}
